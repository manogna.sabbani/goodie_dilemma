// Assumes, the input file has same or more gifts than the no. of employees
// Error Handling not done.. TODO
import 'dart:io';

void main(List<String> args) {
  String inputFile = args[0];
  String outputFile = args[1];
  GoodieDilemma gd = GoodieDilemma(inputFile, outputFile);
  gd.distribute();
}

class GoodieDilemma {
  String inputFile, outputFile;
  int numEmployees;
  var goodies = new Map();

  GoodieDilemma(String inputFile, String outputFile) {
    this.inputFile = inputFile;
    this.outputFile = outputFile;
  }

  void distribute() {
    parseFileContent();
    // print(numEmployees);
    // print(goodies);
    writeGoodiesToDistribute();
    // writeToFile('Output Content');
  }

  void writeGoodiesToDistribute() {
    var valuesList = goodies.values.toList();
    var goodieMinDiff = Map();

    for (var i = 0; i < valuesList.length; i++) {
      var lastIndex = i + numEmployees - 1;
      if (lastIndex >= valuesList.length) break;

      goodieMinDiff["${i}-${lastIndex}"] =
          valuesList[i] - valuesList[lastIndex];
      // print(minRange);
    }

    var sortedEntries = goodieMinDiff.entries.toList()
      ..sort((e1, e2) {
        var diff = e2.value.compareTo(e1.value);
        if (diff == 0) diff = e2.key.compareTo(e1.key);
        return diff;
      });
    MapEntry me = sortedEntries[sortedEntries.length - 1];
    // print(me);
    int start = int.parse(me.key.split('-')[0]);
    int end = int.parse(me.key.split('-')[1]);
    int minDiff = me.value;
    var goodies2Distribute = goodies.entries.toList().sublist(start, end + 1);
    // print(minDiff);
    // print(goodies2Distribute);
    // print(valuesList);

    var sb = StringBuffer();
    sb.write("The goodies selected for distribution are:\n\n");
    goodies2Distribute.reversed.toList().forEach((goodie) {
      sb.write("${goodie.key}: ${goodie.value}\n");
    });

    sb.write(
        "\nAnd the difference between the chosen goodie with highest price and the lowest price is ${minDiff}");

    writeToFile(sb.toString());
    //
  }

  //
  void parseFileContent() {
    File file = new File(inputFile);
    List<String> contents = file.readAsLinesSync();
    numEmployees = int.parse(contents[0].split(': ')[1]);
    contents.sublist(4).forEach((value) {
      goodies[value.split(': ')[0]] = int.parse(value.split(': ')[1]);
    });

    var sortedEntries = goodies.entries.toList()
      ..sort((e1, e2) {
        var diff = e2.value.compareTo(e1.value);
        if (diff == 0) diff = e2.key.compareTo(e1.key);
        return diff;
      });

    goodies
      ..clear()
      ..addEntries(sortedEntries);
  }

  void writeToFile(String content) {
    new File(outputFile).writeAsString(content);
  }
}
